# プラグインスクリーンショット画面

## トップのテストケース管理をクリックしたときの画面

![テストケース管理画面](00-tcm-home.png)

## 新しいテストケースを作成するときの画面

![](01-tcm-new-test-cases.png)

## 新しいテストケースを作成したときの画面(テストケース詳細)

![](02-tcm-test-case-saved.png)

## まだテスト計画していないときの画面

![](03-tcm-no-test-plans.png)

## 新しいテスト計画を作成するときの画面

![](04-tcm-new-test-plan.png)

## テスト計画にテストケースをひもづけるときの画面

![](05-tcm-associate-test-case.png)

## テスト計画にテストケースをひもづけたときの画面

![](06-tcm-test-case-associated.png)


## 新しくテスト実行結果を作成するときの画面

![](07-tcm-new-test-case-executuion.png)

## テスト実行結果詳細表示画面

![](08-tcm-test-case-execution.png)

## テスト計画一覧画面

![](09-tcm-list-test-plans.png)

## テストケース一覧画面

![](10-tcm-list-test-cases.png)

## テスト実行結果一覧画面

![](11-tcm-list-test-case-executions.png)
